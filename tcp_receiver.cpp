#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <pcap.h>
#include <vector>
#include "list.h"
#include "common.h"
#include "tcp_receiver.h"

using namespace std;

int tcp_conn_state = IDLE_STATE;
struct timeval tcp_start, tcp_end;
int numPktsRecvdT = 0;
int totalPacketsT = 0;
int packet_sizeT = 100;
int endOfTransfer = 0;

unsigned int maxSeqNoInList = 0;
long finalPackNo = -1;

int startOfTransfer = 0;

pthread_mutex_t lock=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv_start_timeout;


void  tcp_pkt_receiver (session_t *session) {

	pcap_t * handle, *write_handle1, *write_handle2; // Define a PCAP 
	char dev[MAX_NAME_LENGTH];
	strcpy(dev, "inf000");
	char errbuf[PCAP_ERRBUF_SIZE];	/* Error string */
	char getMac[MAX_NAME_LENGTH];
	char filter_exp[] = "ether src ";		/* filter expression [3] */
	getMacForPcapFilter(session->senderName,getMac,session);
	strcat(filter_exp,getMac);
	printf ("Filter expression:%s\n",filter_exp);

	struct bpf_program fp;			/* compiled filter program (expression) */
	bpf_u_int32 mask;			/* subnet mask */
	bpf_u_int32 net;			/* ip */
	
	/* defing threads here */
	pthread_t pktRcvr,timeoutThread; 
	int err=0;
	void *res; 

	/* defining the wrapper to send it to the Threads */ 
	rcvrSession_t tcpSession;
	memset(&tcpSession, 0 , sizeof(tcpSession));
	tcpSession.expected_seqno = 0;
	
    tcpSession.ses=session;

    // Amogh
	memset(&tcpSession.src_mac, 0, 6);
	dprintf("device name is %s, MAC address is: ",dev );
	populateMAC(dev, tcpSession.src_mac);
	//lookMacAddr(dev, tcpSession.src_mac, tcpSession.ses);
	print_mac_address( tcpSession.src_mac );

	// We need to initialize the list 
    	vector<bool> tcpRcvr; 

	/* get network number and mask associated with capture device */
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "Couldn't get netmask for device %s: %s\n",
		    dev, errbuf);
		net = 0;
		mask = 0;
	}
	     
    /* BUFSIZE not defined , Make sure common.h will have it*/
    handle= pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
    if (handle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(-1);
	}

	if(pcap_setdirection(handle, PCAP_D_IN) == -1)
	{
		fprintf(stderr, "Couldn't set direction for device %s\n", dev);
		exit(-1);	
	}
	
	/* compile the filter expression */
	if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
		fprintf(stderr, "Couldn't parse filter %s: %s\n",
		    filter_exp, pcap_geterr(handle));
		exit(-1);
	}

	/* apply the compiled filter */
	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Couldn't install filter %s: %s\n",
		    filter_exp, pcap_geterr(handle));
		exit(-1);
	}
    
    /* BUFSIZE not defined , Make sure common.h will have it*/
    write_handle1= pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
    if (write_handle1 == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(-1);
	}
	
	 /* BUFSIZE not defined , Make sure common.h will have it*/
    write_handle2= pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
    if (write_handle2 == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(-1);
	}
	
    tcpSession.sniffHandle=handle;
    tcpSession.sniffWriteHandle1=write_handle1;
    tcpSession.sniffWriteHandle2=write_handle2;
    tcpSession.rcvrList=&tcpRcvr;

    /* now Create threads */
    err = pthread_create (&pktRcvr, NULL, &rcvrThread, &tcpSession);
        if (err < 0) {
            fprintf (stderr, "pthread_create: Error , while creating rcvrThread\n");
            return;
    }

    err = pthread_create (&timeoutThread, NULL, &rvcrTimeout, &tcpSession);
        if (err < 0) {
            fprintf (stderr, "pthread_create: Error , while creating rcvrThread\n");
            return;
    }

    err = pthread_join(pktRcvr, &res);
    if (err < 0) {
        fprintf (stderr, "error in pthread_join tcp_controller\n");
        return;
    }

   err = pthread_join(timeoutThread, &res);
    if (err < 0) {
        fprintf (stderr, "error in pthread_join udp_sender\n");
        return;
    }

	pcap_close(handle);
	pcap_close(write_handle1);
	pcap_close(write_handle2);
     return;
}


void* rcvrThread (void *arg) { 

	dprintf("In rcvrThread\n");
	
	rcvrSession_t *rSession= (rcvrSession_t *)arg;
    int err;

	/* now listen on the interface */ 
	while((err=pcap_loop(rSession->sniffHandle, -1, myCallback, (u_char*) arg)) != -1);
	
    if ( err < 0 ) { 
    	
    	fprintf(stderr,"Pcap_loop error : %s\n ",pcap_geterr(rSession->sniffHandle));
    	exit(0);
    }

    return NULL;

}

void myCallback(u_char* arg, const struct pcap_pkthdr *pkthdr,const u_char* packet) { 
	
#ifdef DEBUG	
	static int count = 0;
	count++;
#endif
	
	rcvrSession_t	*rses=(rcvrSession_t *)arg;
	pkt_h *pHeader = (pkt_h*) packet;
	tcp_h* tcpHeader=NULL;
	int setMACs = 0;
	char dev[MAX_NAME_LENGTH];
	strcpy(dev, "inf000");
	/*unsigned char source_mac[ETHER_ADDR_LEN];
	populateMAC(dev, source_mac);
	memcpy(rses->src_mac, source_mac, ETHER_ADDR_LEN);
	*/
	
	dprintf( "In myCallback: received packet [%d], sequence_no [%d]\n", count, ((tcp_h *)((unsigned char*)packet + sizeof(pkt_h)))->seq_no);
	
	/* Now We need to check if packet is already present */

	tcpHeader=(tcp_h*) ((unsigned char *) packet + sizeof(pkt_h));
		
#ifdef RTT_MEASURE
	if(IS_SYN(pHeader))
	{
		dprintf("SYN bit is set\n");
		int sndCnt = 0;
			dprintf ("-----------SYN received!!------------\n")		
			//create SYN+ACK and send
			unsigned char *finAckPkt = NULL;
			unsigned int len = (TYPE1_SIZE + sizeof(tcp_h)+sizeof(pkt_h));
			finAckPkt = (unsigned char *) malloc( sizeof(unsigned char) * len);
					
			memset(finAckPkt, 0, len);
			
			//openflow

		
			pkt_h *p = (pkt_h *) finAckPkt;
			
			memcpy(p->ether_shost, source_mac, ETHER_ADDR_LEN);
			memcpy(p->ether_dhost, pHeader->ether_shost, ETHER_ADDR_LEN);

			if (!setMACs)
			{
				dprintf("Received SYN Packet from:");
				memcpy(rses->des_mac, p->ether_dhost, ETHER_ADDR_LEN);
				print_mac_address(rses->des_mac);
				dprintf("\n");
				setMACs = 1;
			}
					
			unsigned char *first_byte = &p->first;
					
			#ifdef NOTREQ	
			//Set the host bits
			*first_byte = (*first_byte) | 0x40; //node 1
					
			//set the nw bits
			*first_byte = (*first_byte) | 0x10; //network 1
			#endif
					
			//set the packet type bits
			*first_byte = (*first_byte) | 0x08; //Paccket type 1	
					
			//set the flags
			*first_byte = (*first_byte) | 0x03; //Set the SYN (2) ACK (1)  flags

			
		
			dprintf("jsut before the injecting\n");
					
			print_tcp_packet(finAckPkt);
					
			for(;sndCnt < 40; sndCnt++)
			{
						
				// Write the Ethernet frame to the interface.
				if (pcap_inject(rses->sniffWriteHandle1, finAckPkt, len)==-1) {
						pcap_perror(rses->sniffWriteHandle1,0);
						pcap_close(rses->sniffWriteHandle1);
						exit(1);
						}
			}
					
			free(finAckPkt);
			
			return;
	}
#endif
	
	/* TODO: Reject the packets already received */
	if(tcpHeader->seq_no < rses->expected_seqno)
	{
		//ignore, since already received
		return;
	}	 
		
		
	if(!startOfTransfer)
	{
		//set packet size
		if(IS_LARGE_PKT(pHeader))
		{
			packet_sizeT = TYPE1_SIZE;
		}
		
		//Get the start time
		gettimeofday(&tcp_start, NULL);

		//generate a SYN_ACK return

		pthread_mutex_lock(&lock);
		//change the connection state
		tcp_conn_state = ESTABLISHED;
		pthread_mutex_unlock(&lock);
			
		pthread_cond_signal(&cv_start_timeout);
		
		//set flag
		startOfTransfer = 1;
	}
	
	pthread_mutex_lock(&lock);
	/* Packet not present Add it to the List ,Make sure You add at proper place*/ 
	if(tcpHeader->seq_no >= rses->rcvrList->size())
		rses->rcvrList->resize(tcpHeader->seq_no + 100000);
	
	rses->rcvrList->at(tcpHeader->seq_no) = true;
	
	//
	if(IS_FIN(pHeader))
	{
		finalPackNo = tcpHeader->seq_no;
	}
	
	//update the max seq no in list
	if( tcpHeader->seq_no > maxSeqNoInList)
	    	maxSeqNoInList =  tcpHeader->seq_no;
	    	
	pthread_mutex_unlock(&lock);	

	/* 
	    	Now traverse the list to check for Flushing out inorder packet \
	    	and updating the expected_seqno
	*/

	if(rses->expected_seqno >= rses->rcvrList->size())
		rses->rcvrList->resize(rses->expected_seqno + 100000);
	
    	while(rses->rcvrList->at(rses->expected_seqno) == true) 
    	{
    		dprintf( "Packet %u received in order\n", rses->expected_seqno);
		
		    
			
		if(rses->expected_seqno == (unsigned int)finalPackNo)
		{
			dprintf( "FINAL packet!!!\n");
		
			// print the statistics
			printStatisticsT(rses);
							
			pthread_mutex_lock(&lock);
			//set a flag for timeout thread
			endOfTransfer = 1;
			pthread_mutex_unlock(&lock);
					
			int sndCnt = 0;
					
			//create FIN+ACK and send
			unsigned char *finAckPkt = NULL;
			unsigned int len = (TYPE1_SIZE + sizeof(tcp_h)+sizeof(pkt_h));
			finAckPkt = (unsigned char *) malloc( sizeof(unsigned char) * len);
					
			memset(finAckPkt, 0, len);
		
			pkt_h *p = (pkt_h *) finAckPkt;

			//openflow
			memcpy(p->ether_shost, rses->src_mac, ETHER_ADDR_LEN);
			memcpy(p->ether_dhost, rses->des_mac, ETHER_ADDR_LEN);
					
			unsigned char *first_byte = &p->first;
					
			#ifdef NOTREQ	
			//Set the host bits
			*first_byte = (*first_byte) | 0x40; //node 1
					
			//set the nw bits
			*first_byte = (*first_byte) | 0x10; //network 1
			#endif
					
			//set the packet type bits
			*first_byte = (*first_byte) | 0x08; //Paccket type 1	
					
			//set the flags
			*first_byte = (*first_byte) | 0x05; //Set the FIN (4) ACK (1)  flags
		
						
			//print_tcp_packet(finAckPkt);
#if 0					
			for(sndCnt = 0;sndCnt < DUP_SYN_ACKS; sndCnt++)
			{
				dprintf("jsut before the injecting FIN\n");
					
				// Write the Ethernet frame to the interface.
				if (pcap_inject(rses->sniffWriteHandle1, finAckPkt, len)==-1) {
						pcap_perror(rses->sniffWriteHandle1,0);
						pcap_close(rses->sniffWriteHandle1);
						exit(1);
						}
						
				usleep(100);		
			}
#endif			
			
					
			free(finAckPkt);
			
			exit(0);
			
			//exit
			dprintf("Receiver thread exiting !!!\n");
			pthread_exit(0);
			
			pthread_mutex_lock(&lock);
			rses->inOrderPackets++;
			rses->expected_seqno++;
			pthread_mutex_unlock(&lock);
										
			//exit
			dprintf("Receiver thread exiting !!!\n");
			pthread_exit(0);
					
		}
				
			pthread_mutex_lock(&lock);
			rses->inOrderPackets++;
			rses->expected_seqno++;
			
			//Check if the list needs to be expended	
			if(rses->expected_seqno >= rses->rcvrList->size())
				rses->rcvrList->resize(rses->expected_seqno + 100000);
				
			pthread_mutex_unlock(&lock);
			
			
		
    	}//end of while loop
    		
	

    return;
}




/* 
	Thread takes care of sending NAKs to the tcp sender 
*/
void* rvcrTimeout (void *arg) { 

	rcvrSession_t *rses= (rcvrSession_t *)arg;
	unsigned char *pkt = NULL;
	pkt_h *p = NULL;
	tcp_h *t = NULL;
	unsigned int len = 0, expectedSeqNo = 0;
	char dev[MAX_NAME_LENGTH];
	strcpy(dev, "inf000");

	
	//Wait until a packet has been received over UDP
	// to fetch sender's address before begin sending

	pthread_mutex_lock(&lock);
	while (tcp_conn_state != ESTABLISHED) {
		pthread_cond_wait(&cv_start_timeout, &lock);
	}
	pthread_mutex_unlock(&lock);
	
	dprintf("rvcrTimeout started\n");
	
	//populateMAC(dev, );
	
	dprintf("device name is %s, MAC address is",dev);
	print_mac_address(rses->src_mac);
		
	dprintf("Dest MAC address is: ");
	print_mac_address(rses->des_mac);	
	dprintf("\n");
	
	while(1)
	{
		//Check if the signal to stop has been received
		pthread_mutex_lock(&lock);		
		if(endOfTransfer == 1)
		{
			pthread_mutex_unlock(&lock);
            	dprintf("Calling pthread_exit from function rvcrTimeout !!!\n");

			pthread_exit(0);
		}
		pthread_mutex_unlock(&lock);

		// Sleep for some time
		usleep(TCP_TIMEOUT_SLEEP);

		pthread_mutex_lock(&lock);

		expectedSeqNo = rses->expected_seqno;
		dprintf("Next expected sequence no is  - %d.\n", expectedSeqNo);		
		pthread_mutex_unlock(&lock);
		
		len = TYPE1_SIZE+sizeof(tcp_h)+sizeof(pkt_h);
		pkt = (unsigned char *) malloc(sizeof(unsigned char) * len);
		
		if(pkt == NULL)
		{
			fprintf(stderr, "Malloc failed!!!\n");
			exit(-1);
		}
				
		memset(pkt, 0, len);
		
		p = (pkt_h *) pkt;

		t = (tcp_h *) ((unsigned char *)p + sizeof(pkt_h));
		
		unsigned char *first_byte = &p->first;
		unsigned char *payload = (unsigned char *) (pkt + sizeof(pkt_h) + sizeof(tcp_h));
		 
#ifdef NOTREQ	
		//Set the host bits
		*first_byte = (*first_byte) | 0x40; //node 1
		
		//set the nw bits
		*first_byte = (*first_byte) | 0x10; //network 1
#endif
		
		//set the packet type bits
		*first_byte = (*first_byte) | 0x08; //Paccket type 1	
		
		//set the flags
		*first_byte = (*first_byte) | 0x01; //Set the ACK flag
		
		//set tcp header
		t->seq_no = expectedSeqNo;
		

		//copy the max seq no in list int payload + 7
		pthread_mutex_lock(&lock);
		memcpy((unsigned char *)payload + 7, (unsigned char *)&maxSeqNoInList, sizeof(unsigned int));
		int curPos = 7 + sizeof(unsigned int);
		unsigned int curSeqNo = 0;
		
		//if list is empty, that means expectedseqno > maxseqnoinlist
		
		if(expectedSeqNo > maxSeqNoInList)
			goto DoneWithPayload;
    		
 			
    		for(	curSeqNo = expectedSeqNo;  curSeqNo < maxSeqNoInList; curSeqNo++)
    		{	
    				if((curPos + (curSeqNo - expectedSeqNo)) > TYPE1_SIZE - 2)
    					goto DoneWithPayload;
    					
    				if(rses->rcvrList->at(curSeqNo) == false)
    					* (payload + (curPos + (curSeqNo - expectedSeqNo))) = 1;
    				
    		}
   		
    		
		DoneWithPayload:
		
		pthread_mutex_unlock(&lock);

	
		//openflow
		memcpy(p->ether_shost, rses->src_mac, ETHER_ADDR_LEN);
		memcpy(p->ether_dhost, rses->des_mac, ETHER_ADDR_LEN);

		//send the packet
		dprintf("jsut before the injecting\n");
		
		print_tcp_packet(pkt);
		
		
		// Write the Ethernet frame to the interface.
		if (pcap_inject(rses->sniffWriteHandle2, pkt, len)==-1) {
			pcap_perror(rses->sniffWriteHandle2,0);
			pcap_close(rses->sniffWriteHandle2);
			exit(1);
		}
		
		if(pkt != NULL)
			free(pkt);
	}

	return NULL;

}


void printStatisticsT(void *arg)
{
	dprintf("In printStatisticsT\n");
	
	rcvrSession_t	*rses=(rcvrSession_t *)arg;
	long elapsed_usec;
	double elapsed_sec;
	double throughput;
	long transfer_bytes;
	double transfer_mbytes;
	
	//calculate end time
	gettimeofday(&tcp_end, NULL);

	//calculate time elapsed
	elapsed_usec = (tcp_end.tv_usec - tcp_start.tv_usec) + SECTOUSEC * ( tcp_end.tv_sec - tcp_start.tv_sec );
	elapsed_sec = (double) elapsed_usec / (double) SECTOUSEC;


	dprintf("Packet Size--> %u\n", packet_sizeT);
	dprintf("Inorder Packets--> %u\n", rses->inOrderPackets);
	
	//Total packets received
	throughput = ((double) (rses->inOrderPackets * packet_sizeT * 8) / (double) elapsed_sec) / (double) SECTOUSEC;
	transfer_bytes =  rses->inOrderPackets * packet_sizeT;
	transfer_mbytes = (double) transfer_bytes / (double) SECTOUSEC;

	printf("Interval\tTrasfer         Bandwidth\n");
	printf("%.01lf sec\t%.02lf MBytes\t%.02lf Mbits/sec\n", elapsed_sec, transfer_mbytes, throughput);

}
