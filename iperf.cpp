#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "common.h"

void print_usage()
{
	fprintf(stderr, "iperf [-c <server> |-s <client>] [-d <numSecs>] [-n <numPkts>] [-l 0|1: 100? 1500] [-u] \n");
}

int parse_cmdline(int argc, char *argv[], session_t *pSession)
{
	char *cvalue = NULL;
	int index;
	int c;

	opterr = 0;
	while ((c = getopt (argc, argv, "c:s:d:l:n:u")) != -1)
		switch (c)
		{
		case 'c':
			cvalue = optarg;
			pSession->mode = SENDER_MODE;
			strcpy(pSession->receiverName, cvalue);
			lookMacAddr(pSession->receiverName,pSession->receiverMac,pSession);
			fprintf(stdout,"Sender Mode set, server name: %s\n", cvalue);
			break;
		case 's':
			cvalue = optarg;
			pSession->mode = RECEIVER_MODE;
			strcpy(pSession->senderName, cvalue);
			lookMacAddr(pSession->senderName,pSession->senderMac,pSession);
			fprintf(stdout,"Receiver mode set, client name: %s\n", cvalue);
			break;
		case 'd':
			cvalue = optarg;
			pSession->duration = atoi(cvalue);
			fprintf(stdout,"Set duration = %d seconds\n", pSession->duration);
			break;
		case 'l':
			cvalue = optarg;
			if (atoi(cvalue) == 0)
			{
				pSession->packetSize = TYPE0_SIZE;
				fprintf(stdout,"Set packetSize = %d Bytes\n", pSession->packetSize);
			}
			else
			{
				pSession->packetSize = TYPE1_SIZE;
				fprintf(stdout,"Set packetSize = %d Bytes\n", pSession->packetSize);
			}
			break;
		case 'n':
			cvalue = optarg;
			pSession->numPkts = atoi(cvalue);
			fprintf(stdout,"Number of packets to send = %d\n", pSession->numPkts);
			break;
		case 'u':
			pSession->protocol = UDP_PROTO;
			fprintf(stdout,"protocol set to UDP(default is TCP)\n");
			break;
		case '?':
			if (optopt == 'c' || optopt == 'd' || optopt == 'l' || optopt == 'n')
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint (optopt))
				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf (stderr,
						"Unknown option character `\\x%x'.\n",
						optopt);
			return 1;
		default:
			abort();
		}

	/*printf ("aflag = %d, bflag = %d, cvalue = %s\n",
           aflag, bflag, cvalue);
	 */
	for (index = optind; index < argc; index++)
		fprintf (stdout, "Non-option argument %s\n", argv[index]);

	//validation of arguments
	/*
    1. if both duration and numpackets are not provided
        set duration = 10 use it
    2. if both are set, flag error and return
	 */
	if (pSession->duration && pSession->numPkts ) {
		fprintf(stderr, "Use either -n or -d only, not both!\n");
		return -1;
	}
	if (pSession->duration == 0 && pSession->numPkts == 0) {
		pSession->duration = 10;    // default 10 seconds
	}

	return 0;
}

void printMacTableEntries(session_t *pSession)
{
	unsigned int i=0;
	fprintf(stdout,"Number of Mac table entries %u\n",pSession->num_of_mac_entries);
	for (i=0;i<pSession->num_of_mac_entries;i++)
	{
		fprintf(stdout,"Mac table entry %d\n",i);
		fprintf(stdout,"Nodename %s\n", pSession->macTable[i].nodeName);
		print_mac_address(pSession->macTable[i].macAddr);
	}
	return;
}

void printSessionValues(session_t *pSession)
{
	fprintf(stdout,"Session Parameters:\n");
	printMacTableEntries(pSession);
	fprintf(stdout,"Current sequence number:%u\n",pSession->cur_seq_no);
	fprintf(stdout,"Duration:%u\n",pSession->duration);
	fprintf(stdout,"Mode set:%u (SENDER_MODE-1 RECEIVER_MODE-0)\n",pSession->mode);
	fprintf(stdout,"Number of packets to be sent:%u\n",pSession->numPkts);
	fprintf(stdout,"Number of packets sent so far:%u\n",pSession->numPktsSent);
	fprintf(stdout,"Packet size:%u(0 -100 bytes 1-1500 bytes)\n",pSession->packetSize);
	fprintf(stdout,"Protocol to be used:%u\n",pSession->protocol);
	fprintf(stdout,"Receiver Name:%s\n",pSession->receiverName);
	fprintf(stdout,"Receiver MAC address:");
	print_mac_address(pSession->receiverMac);
	fprintf(stdout,"Sender Name:%s\n",pSession->receiverName);
	fprintf(stdout,"Sender MAC address:");
	print_mac_address(pSession->senderMac);
	return;
}

int main (int argc, char **argv)
{
	session_t session;

	memset(&session, 0, sizeof(session_t));
	//print_usage();

	fillMacTable(&session);
	if (parse_cmdline(argc, argv, &session) < 0 )
		return 0;
	//printSessionValues(&session);

	if (TCP_PROTO == session.protocol && SENDER_MODE == session.mode) {
		fprintf(stdout,"MultiNAK TCP Sender started\n");
		//fillMacTable(&session);
		tcp_pkt_sender(&session);
		return 0;
	}

	if (UDP_PROTO == session.protocol && SENDER_MODE == session.mode) {
		fprintf(stdout,"Invoking UDP Sender\n");
		//fillMacTable(&session);
		udp_send(&session);
		// call UDPSender();
		return 0;
	}

	if (TCP_PROTO == session.protocol && RECEIVER_MODE == session.mode) {
		fprintf(stdout,"MultiNAK TCP Receiver started\n");
		//fillMacTable(&session);
		tcp_pkt_receiver(&session);
		return 0;
	}

	if (UDP_PROTO == session.protocol && RECEIVER_MODE == session.mode) {
		fprintf(stdout,"Invoking UDP Receiver\n");
		//fillMacTable(&session);
		udp_receiver(&session);
		// call UDPReceiver();
		return 0;
	}
	return 0;
}
