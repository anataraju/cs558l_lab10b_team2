#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pcap.h>
#include <pthread.h>
#include <unistd.h>
#include "common.h"

//Global variables

pthread_t ack_thread;
unsigned int pkt_len;
unsigned int pkt_seq_no;
pthread_mutex_t tcplock;
pthread_cond_t tcphack;
unsigned long rtt_val;
struct timeval tbegin, tend;
int rtt_measure_taken =0;
int signal_send_start = 0;
unsigned char tcps_src_mac[6];
unsigned char tcps_dst_mac[6];

pcap_t * listener_write_handle;

int isSet(int x, unsigned char* bytemap)
{
	return (int) *((unsigned char*)bytemap + x);
}


void tcp_send_callback(u_char *args, const struct pcap_pkthdr* pkthdr,const u_char* packet)
{
	pkt_h *packet_details=NULL;	
	unsigned int j;

	tcp_h *tcp_details=NULL;
	unsigned char *pkt= NULL;
	session_t *pSession = NULL;
	unsigned int start_seq_no;
	unsigned int last_seq_no;
	pSession = (session_t*)args;

	packet_details = (pkt_h*)(packet);
	tcp_details = (tcp_h *)(packet + sizeof(pkt_h));

	unsigned char *bytemap, *payl;
	//bytemap = (unsigned char *) malloc(sizeof(unsigned char) *( TYPE1_SIZE - (7 + sizeof(int)))); /* size we can decide */


	dprintf("in tcp_send_callback\n");	


	//check if it is an ack packet
	if(IS_ACK(packet_details))
	{	
		
		dprintf("Now Processing Ack !\n");
#ifdef RTT_MEASURE
		if(IS_SYN(packet_details))
		{
			if(!rtt_measure_taken)
			{	

				pthread_mutex_lock(&tcplock);
				gettimeofday(&tend, NULL);

				rtt_val = (tend.tv_usec - tbegin.tv_usec) + (tend.tv_sec - tbegin.tv_sec) * 1000000;
				rtt_measure_taken = 1;
				pthread_cond_signal(&tcphack);
				pthread_mutex_unlock(&tcplock);
			}	

		}
		/* put hacck for single ack */
		else if(!IS_FIN(packet_details) /*&& (rtt_val < 100000) && (pSession->packetSize == TYPE1_SIZE)*/)
			
#else
	/* put hacck for single ack */
		if(!IS_FIN(packet_details) /*&& (rtt_val < 100000) && (pSession->packetSize == TYPE1_SIZE)*/)
		
#endif
		{
#ifdef SINGLE_ACK_PART
		

			dprintf("Entered SINGLE-ACKS block\n");

			dprintf("Processing the ACK PAcket !!\n");
			//exclude the 7 bytes and start reading the next 8 bytes for start_seq_no and last_seq_no
			payl = (unsigned char *)( (unsigned char*)tcp_details + sizeof(tcp_h));
			bytemap = (unsigned char *)payl +  sizeof(unsigned int);

			start_seq_no = tcp_details->seq_no;

			memcpy(&last_seq_no, payl, sizeof(int));

			dprintf("Received NACK packet\n");

			//traverse the byte vector ans look for set bytes and send seq_no accordingly
			//for(j=start_seq_no; j < numPktsSent; j++)
			j=start_seq_no;
			{
				/* set FIN if last */
				if(j == (pSession->numPkts - 1))
				{
					pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,1 /* FIN bit */, 0,0,j,pSession->numPkts,&pkt, tcps_dst_mac, tcps_src_mac);
					dprintf("Packet length: %u\n", pkt_len);
				}
				else
				{
					pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,0, 0,0,j,pSession->numPkts,&pkt, tcps_dst_mac, tcps_src_mac);
					dprintf("Packet length: %u\n", pkt_len);
				}

				//print_tcp_packet(pkt);

				if (pcap_inject(listener_write_handle, pkt, pkt_len)==-1)
				{
					pcap_perror(listener_write_handle, 0);
					pcap_close(listener_write_handle);
					exit(0);
				}

				dprintf("FIRST--> sending packet with sequence no- %d\n", ((tcp_h *)((unsigned char*)pkt + sizeof(pkt_h)))->seq_no);
#ifdef 	TCP_INTERPKT_DELAY_RETRANS				
				usleep(TCP_INTERPKT_DELAY_RETRANS);
#endif					
			}
			
#endif /* SINGLE_ACK_PART */
}
#ifdef MULTI_ACK_PART
		if(!IS_FIN(packet_details))
		{	

			dprintf("Entered MULTIPLE-ACKS block\n");

			//Create local variables
			unsigned int numPktsSent = 0;

			pthread_mutex_lock(&tcplock);
			numPktsSent = pSession->numPktsSent;
			pthread_mutex_unlock(&tcplock); 	

			dprintf("Processing the ACK PAcket !!\n");
			//exclude the 7 bytes and start reading the next 8 bytes for start_seq_no and last_seq_no
			payl = (unsigned char *)( (unsigned char*)tcp_details + sizeof(tcp_h));
			bytemap = (unsigned char *)payl +  sizeof(unsigned int);

			start_seq_no = tcp_details->seq_no;

			memcpy(&last_seq_no, payl, sizeof(int));

			dprintf("Received NACK packet\n");
			dprintf("Total packets sent - %u\n", numPktsSent);
			//dprintf( "Last seq no - %u\n", last_seq_no);
			//dprintf( "start seq no - %u\n", start_seq_no);

			if(start_seq_no > last_seq_no)
			{	
				dprintf("first\n");
				last_seq_no = numPktsSent;

				//traverse the byte vector ans look for set bytes and send seq_no accordingly
				for(j=start_seq_no; j < numPktsSent; j++)
					//j=start_seq_no;
				{
					/* set FIN if last */
					if(j == (pSession->numPkts - 1))
					{	
						pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,1 /* FIN bit */, 0,0,j,pSession->numPkts,&pkt, tcps_dst_mac, tcps_src_mac);
						dprintf("Packet length: %u\n", pkt_len);
					}
					else
					{
						pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,0, 0,0,j,pSession->numPkts,&pkt, tcps_dst_mac, tcps_src_mac);
						dprintf("Packet length: %u\n", pkt_len);
					}

					print_tcp_packet(pkt);

					if (pcap_inject(listener_write_handle, pkt, pkt_len)==-1)
					{
						pcap_perror(listener_write_handle, 0);
						pcap_close(listener_write_handle);
						exit(0);
					}

					dprintf("FIRST--> sending packet with sequence no- %d\n", ((tcp_h *)((unsigned char*)pkt + sizeof(pkt_h)))->seq_no);
#ifdef 	TCP_INTERPKT_DELAY_RETRANS				
					usleep(TCP_INTERPKT_DELAY_RETRANS);
#endif					
				}
			}

			else
			{		
				dprintf("second\n");
				//traverse the byte vector ans look for set bytes and send seq_no accordingly

				for(j=0;j<(last_seq_no - start_seq_no);j++)
				{

					if(j == (TYPE1_SIZE -4 /* unused 7 + 4 for int */ -1))
						break;

					//dprintf("Bytemap - %d\n", *((unsigned char *)bytemap + j));

					//call isSet function
					if(isSet(j,bytemap))
					{
						//send packet with the seq no equal to the ack no
						pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,0, 0,0,(start_seq_no+j),0,&pkt, tcps_dst_mac, tcps_src_mac);
						dprintf("Packet length: %u\n", pkt_len);

						print_tcp_packet(pkt);

#ifdef TCP_SENDER_NUM_COPIES_RETRANS
						int j = 0;
						while(j <=TCP_SENDER_NUM_COPIES)
						{
#endif													
							if (pcap_inject(listener_write_handle, pkt, pkt_len)==-1)
							{
								pcap_perror(listener_write_handle, 0);
								pcap_close(listener_write_handle);
								exit(0);
							}
#ifdef TCP_SENDER_NUM_COPIES_RETRANS
							j++;
						}
#endif						

						dprintf( "SECOND--> sending packet with sequence no- %d\n", ((tcp_h *)((unsigned char*)pkt + sizeof(pkt_h)))->seq_no);
						dprintf( "Last seq no - %u\n", last_seq_no);
						dprintf( "start seq no - %u\n", start_seq_no);
#ifdef TCP_INTERPKT_DELAY_RETRANS			
						usleep(TCP_INTERPKT_DELAY_RETRANS);
#endif	
					}

				}



			}

		}
#endif /*MULTI_ACK_PART */
		else
		{
			dprintf("tcp_send_callback thread is exiting!!!\n");	
			pthread_exit(0);
		}	
	}

}

void *listen_for_acks(void *args)
{

	pcap_t *read_handle; // handle to sniff packets
	char *dev = NULL; 				// device to sniff on and send packets
	char errbuf[PCAP_ERRBUF_SIZE]; //error string
	session_t *pSession=NULL;
	pSession=(session_t *)args;

	char getMac[MAX_NAME_LENGTH];
	char filter_exp[] = "ether src ";		/* filter expression [3] */
	getMacForPcapFilter(pSession->receiverName,getMac,pSession);
	strcat(filter_exp,getMac);
	printf ("Filter expression:%s\n",filter_exp);

	struct bpf_program fp;
	bpf_u_int32 mask;			
	bpf_u_int32 net;

	dprintf("In Listening thread\n");

	dev = (char *)malloc(MAX_NAME_LENGTH);
	strcpy(dev,"inf000");

	
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1)
	{
		fprintf(stderr, "Couldn't get netmask for device %s: %s\n",dev, errbuf);
		net = 0;
		mask = 0;
	}

	// open a pcap live session on passed interface
	read_handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (read_handle == NULL)
	{
		fprintf(stderr, "Couldn't open handle for device %s: %s\n", dev, errbuf);
		exit(0);
	}

	listener_write_handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (listener_write_handle == NULL)
	{
		fprintf(stderr, "Couldn't open write handle for device %s: %s\n", dev, errbuf);
		exit(0);
	}

	if(pcap_setdirection(read_handle, PCAP_D_IN) == -1)
	{
		fprintf(stderr, "Couldn't set direction for device %s\n", dev);
		exit(-1);	
	}

	// compiling the filter expression
	if (pcap_compile(read_handle, &fp, filter_exp, 0, net) == -1)
	{
		fprintf(stderr, "Couldn't parse filter %s: %s\n",filter_exp, pcap_geterr(read_handle));
		exit(0);
	}


	// applying the filter
	if (pcap_setfilter(read_handle, &fp) == -1)
	{
		fprintf(stderr, "Couldn't install filter %s: %s\n",filter_exp, pcap_geterr(read_handle));
		exit(0);
	}

	//call pcap_loop() which calls my_callback () after sniffing every packet
	while(pcap_loop(read_handle,-1, tcp_send_callback,(u_char *)pSession) != -1);

	pcap_close(listener_write_handle);
	return NULL;
}

//TCP Sender
void tcp_pkt_sender(session_t *pSession)
{

	char errbuf[PCAP_ERRBUF_SIZE]; //error string
	unsigned int i=0;
	unsigned char* pkt= NULL;
	pcap_t *write_handle;		//handle to send 
	char *dev = NULL; 				// device to sniff on and send packets
	int err=0;
	void *res; 

	pkt_seq_no =0 ; // starting the pkt sequence # from 2 as syn has 1 as pkt_seq_no
	pSession->numPktsSent = 0;

	dev = (char *)malloc(MAX_NAME_LENGTH);
	strcpy(dev, "inf000");

	populateMAC(dev, tcps_src_mac);

	dprintf("device name is %s, MAC address is",dev);
	print_mac_address(tcps_src_mac);


	//get dest mac address
	if(lookMacAddr(pSession->receiverName, tcps_dst_mac, pSession) == -1)
	{	
		fprintf(stderr, "Dest mac lookup failed!!!");
		exit(-1);
	}	

	dprintf("Dest MAC address is: ");
	print_mac_address(tcps_dst_mac);

	// open a pcap live session on passed interface
	/* Open the session in promiscuous mode */
	write_handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (write_handle == NULL)
	{
		fprintf(stderr, "Couldn't open handle for device %s: %s\n", dev, errbuf);
		exit(0);
	}

	err = pthread_create (&ack_thread, NULL, &listen_for_acks, (void *) pSession);
	if (err < 0) {
		fprintf (stderr, "pthread_create: Error , while creating listen_for_acks\n");
		return;
	}

#ifdef RTT_MEASURE
	usleep(1000);

	dprintf("Sending SYN packet\n");
	int j = 0;
	while(j <= 10)
	{
		pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,0 ,1/* SYN */,0,0,pSession->numPktsSent,&pkt, tcps_dst_mac, tcps_src_mac);
		dprintf("Length of packet:%d",pkt_len);
		print_tcp_packet(pkt);
		if (pcap_inject(write_handle, pkt, pkt_len)==-1)
		{
			pcap_perror(write_handle, 0);
			pcap_close(write_handle);
			exit(0);
		}
#ifdef TCP_INTERPKT_DELAY_DUPS					
		usleep(TCP_INTERPKT_DELAY_DUPS);
#endif					
		j++;
	}	

	gettimeofday(&tbegin, NULL);

	pthread_mutex_lock(&tcplock);
	while(rtt_measure_taken != 1)
	{
		pthread_cond_wait(&tcphack, &tcplock);
	}	
	pthread_mutex_unlock(&tcplock);


	printf("The RTT value is %ld\n", rtt_val);

#endif
	dprintf("About to send data packets\n");
	//run a for loop depending upon the number of packets or the duration

	for(i=0;i<pSession->numPkts;i++)
	{
		pSession->cur_seq_no = pkt_seq_no; //updating the current seq no in the session

		/* SEND FIN */
		if(i == pSession->numPkts - 1)
		{
			dprintf("Sending FIN packet\n");
			int j = 0;
			while(j <= 10)
			{
				pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,1 /* FIN */,0,0,pkt_seq_no,pSession->numPktsSent,&pkt, tcps_dst_mac, tcps_src_mac);
				if (pcap_inject(write_handle, pkt, pkt_len)==-1)
				{
					pcap_perror(write_handle, 0);
					pcap_close(write_handle);
					exit(0);
				}
#ifdef TCP_INTERPKT_DELAY_DUPS
				usleep(TCP_INTERPKT_DELAY_DUPS);
#endif
				j++;
			}
		}
		else
		{
			//if((rtt_val > 100000) && (pSession->packetSize == TYPE0_SIZE))
			{
#ifdef TCP_SENDER_NUM_COPIES
				int j = 0;
				while(j <=TCP_SENDER_NUM_COPIES)
				{
#endif
					pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,0,0,0,pkt_seq_no,pSession->numPktsSent,&pkt, tcps_dst_mac, tcps_src_mac);
					if (pcap_inject(write_handle, pkt, pkt_len)==-1) 
					{	
						pcap_perror(write_handle, 0);
						pcap_close(write_handle);
						exit(0);
					}
#ifdef TCP_SENDER_NUM_COPIES
					j++;
				}
#endif
			}/* end of if((rtt_val > 100000) && (pSession->packetSize == TYPE0_SIZE)) */
#if 0
			else
			{
				pkt_len = tcp_make_packet((pSession->packetSize == TYPE1_SIZE)?1:0,0,0,0,pkt_seq_no,pSession->numPktsSent,&pkt, tcps_dst_mac, tcps_src_mac);
				if (pcap_inject(write_handle, pkt, pkt_len)==-1)
				{	
					pcap_perror(write_handle, 0);
					pcap_close(write_handle);
					exit(0);
				}
			}
#endif
		}

		pthread_mutex_lock(&tcplock);
		pkt_seq_no++;
		pSession->numPktsSent++; //increment the number of pkts sent
		pthread_mutex_unlock(&tcplock);
#ifdef TCP_INTERPKT_DELAY			

		if(!(pSession->numPkts % TCP_SLEEP_AFTER))
			usleep(TCP_INTERPKT_DELAY);
#endif
	}


	//Done with sending, now listen for ACKS
	//listen_for_acks((void *) pSession);

	err = pthread_join(ack_thread, &res);
	if (err < 0) {
		fprintf (stderr, "error in pthread_join listen_for_acks\n");
		return;
	}

	pSession->tcp_state= CLOSED;

	return;
}

